DOMAIN=sung-ho.pe.kr
HOSTED_ZONE_ID=ZR5TLNRP2Q72H
PATH=$PATH:/usr/bin:/bin:/usr/local/bin

cd /root/route53/

awk -v URL=`hostname`.${DOMAIN} -v IP=`externalip` '{gsub("_URL_",URL); gsub("_IP_",IP); print $0}' change-resource-record-sets.template > change-resource-record-sets.json

aws route53 change-resource-record-sets \
--hosted-zone-id ${HOSTED_ZONE_ID} \
--change-batch 'file:///root/route53/change-resource-record-sets.json'
