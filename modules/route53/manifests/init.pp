class route53 {
  package { [
    'python-pip',
    'dnsutils',
    ]:
    ensure => 'installed',
  }

  package { "awscli":
    ensure => 'present',
    provider => "pip"
  }

  file { '/root/route53':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0750',
  }

  file { '/root/.aws':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/root/.aws/config':
    source => 'puppet:///modules/route53/config',
    mode => '0600',
    owner => root,
    group  => root,
  }

  file { '/root/.aws/credentials':
    source => 'puppet:///modules/route53/credentials',
    mode => '0600',
    owner => root,
    group  => root,
  }

  file { '/root/route53/change-resource-record-sets.sh':
    source => 'puppet:///modules/route53/change-resource-record-sets.sh',
    mode => '0750',
    owner => root,
    group  => root,
  }

  file { '/root/route53/change-resource-record-sets.template':
    source => 'puppet:///modules/route53/change-resource-record-sets.template',
    mode => '0640',
    owner => root,
    group  => root,
  }

  cron { 'puppet.cron - change-resource-record-sets.sh':
    command => '/root/route53/change-resource-record-sets.sh',
    user    => 'root',
    hour    => '*',
    minute  => '*/15',
  }

}
