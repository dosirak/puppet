class base {
  class { '::ntp':
    servers => [ 'time1.google.com', 'time2.google.com', 'time3.google.com', 'time4.google.com' ],
  }

  file { '/usr/local/bin/externalip':
    source => 'puppet:///modules/base/externalip',
    mode => '0755',
    owner => root,
    group  => root,
  }
  file { '/usr/local/bin/internalip':
    source => 'puppet:///modules/base/internalip',
    mode => '0755',
    owner => root,
    group  => root,
  }
  file { '/usr/local/bin/dfm':
    source => 'puppet:///modules/base/dfm.sh',
    mode => '0755',
    owner => root,
    group  => root,
  }
  file { '/usr/local/bin/fp':
    source => 'puppet:///modules/base/fp.sh',
    mode => '0755',
    owner => root,
    group  => root,
  }
  file { '/usr/local/bin/g':
    source => 'puppet:///modules/base/g.sh',
    mode => '0755',
    owner => root,
    group  => root,
  }
  file { '/usr/local/bin/bk':
    source => 'puppet:///modules/base/bk.sh',
    mode => '0755',
    owner => root,
    group  => root,
  }
  file { '/usr/bin/tcpping':
    source => 'puppet:///modules/base/tcpping',
    mode => '0755',
    owner => root,
    group  => root,
  }
  file { '/usr/local/bin/git_rollback':
    source => 'puppet:///modules/base/git_rollback.sh',
    mode => '0755',
    owner => root,
    group  => root,
  }
  package { [ 'xterm', 'bc', 'tcptraceroute' ]:
    ensure => 'installed',
  }
  file_line { 'append set -o vi':
    path => '/etc/bash.bashrc',
    line => 'set -o vi',
    match => '^set -o vi',
  }
  file_line { 'append alias lr':
    path => '/etc/bash.bashrc',
    line => 'alias lr="ls -lrt"',
    match => '^alias lr=',
  }
  file_line { 'append alias ll':
    path => '/etc/bash.bashrc',
    line => 'alias ll="ls -al"',
    match => '^alias ll=',
  }
  file_line { 'export HISTTIMEFORMAT':
    path => '/etc/bash.bashrc',
    line => 'export HISTTIMEFORMAT="%Y-%m-%d_%H:%M:%S "',
    match => '^export HISTTIMEFORMAT',
  }
  file_line { 'root .bashrc xterm-color 256color':
    path => '/root/.bashrc',
    line => '    xterm-color|*-256color) color_prompt=yes;;',
    match => '^    xterm-color.*color_prompt=',
  }
}
