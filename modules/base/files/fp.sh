function FP {
        PREFIX=`hostname`:

        while [ $# -gt 0 ]
        do
                case $1 in
                -n)
                        PREFIX=""
                        ;;
                *)
                        if [ -s $1 ]
                        then
                                if [ `echo $1 | cut -c1` = "/" ]
                                then
                                        echo ${PREFIX}`dirname $1`/`basename $1`
                                else
                                        DIRFN=`pwd`/`dirname $1`/`basename $1`
                                        echo $DIRFN | awk -v PREFIX=${PREFIX} -F/ '{
                                        for(i=NF;i>0;i--) {
                                                if ($i==".") { continue }
                                                if ($i=="..") { SKIP+=2 }
                                                if (SKIP<1) { if (MSG=="") {MSG=$i} else {MSG=$i"/"MSG} }
                                                if (SKIP>0) { SKIP-- }
                                                }
                                        } END {
                                                print PREFIX""MSG
                                        }'
                                fi
                        fi
                        ;;
                esac

                shift
        done
}

FP $*
