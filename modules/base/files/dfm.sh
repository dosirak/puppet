COL1=$((`df -hm | awk '/^\// {print length($1)}' | sort -n | tail -1`))
[ $COL1 -lt 10 ] && COL1=10
COL2=$((`df -hm | awk '/^\// {print length($2)}' | sort -n | tail -1`))
[ $COL2 -lt 5 ] && COL2=5
COL3=$((`df -hm | awk '/^\// {print length($3)}' | sort -n | tail -1`))
[ $COL3 -lt 6 ] && COL3=6
COL4=$((`df -hm | awk '/^\// {print length($4)}' | sort -n | tail -1`))
[ $COL4 -lt 7 ] && COL4=7
COL5=$((`df -hm | awk '/^\// {print length($5)}' | sort -n | tail -1`))
[ $COL5 -lt 3 ] && COL5=3

function dfm {
LANG=C df -hm | awk -v COL1=$COL1 -v COL2=$COL2 -v COL3=$COL3 \
	-v COL4=$COL4 -v COL5=$COL5 'BEGIN {
	printf("%-"COL1"s  %"COL2"s  %"COL3"s  %"COL4"s  %"COL5"s  %s\n", "Filesystem","TotMB","UsedMB","AvailMB","PCT","Mount","Flag");
} /^\// {
	TOT=$2;
	FREE=$4;
	USED=TOT-FREE;

	if (USED/TOT>.91) {
		MARK="(*)"
	} else {
		MARK=""
	}
	printf("%-"COL1"s  %"COL2"d  %"COL3"d  %"COL4"d  %"COL5"d  %s %s\n",$1,TOT,(TOT-FREE),FREE,USED/TOT*100,$6,MARK);}'
}

dfm
