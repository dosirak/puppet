function PSEF_CODE {
        if [ $# -eq 0 ]
        then
                COLUMNS=9999 ps -ef | grep -iyv "ps -ef" | grep -v "grep -iy"
        else
                case $1 in
                PID)
                        shift
                        PSEF_CODE $* | awk '{print $2}'
                        ;;
                USER)
                        shift
                        USERNAME="$1"; shift
                        PSEF_CODE $* | awk -v USERNAME=$USERNAME 'tolower($1)~tolower(USERNAME)'
                        ;;
                PROC)
                        shift
                        STR="$1"; shift
                        PSEF_CODE $* | grep -iy "$STR"
                        ;;
        COUNT)
            shift
            PSEF_CODE $* | wc -l
            ;;      
                esac
        fi
}

function GREP_PROC {
        PID_OPT=""
        USER_OPT=""
        USER_NAME=""

        while [ $# -gt 0 ]
        do
                case $1 in
                -c)
                        shift
                        CMD_LIST="COUNT ${CMD_LIST}"
                        ;;
                -p)
                        shift
                        CMD_LIST="PID ${CMD_LIST}"
                        ;;
                -u)
                        shift
                        USERNAME=$1; shift
                        CMD_LIST="${CMD_LIST} USER ${USERNAME}"
                        ;;
                *)
                        CMD_LIST="${CMD_LIST} PROC $1"
                        shift
                        ;;
                esac
        done

        CMD_LIST="`echo $CMD_LIST | sed \"s/PID COUNT/COUNT PID/\"`"
        PSEF_CODE ${CMD_LIST}
}

GREP_PROC $*
