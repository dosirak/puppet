function BK {
# 1.0.0
    if [ $# -eq 0 ]
    then
        echo "Usage : `basename $0` filename ..."
        exit 1
    fi
   
    while [ $# -gt 0 ]
    do
        FN=$1 && shift
        FN_RENAME=${FN}.`date +"%Y%m%d.%H%M%S"`
        
        cp -pf ${FN} ${FN_RENAME}
        
        if [ $? -eq 0 ]
        then
            echo "The file \"${FN}\" backuped to \"${FN_RENAME}\" Successfully."
        else
            echo "The file \"${FN}\" backuped to \"${FN_RENAME}\" Failed!!!"
        fi
    done
}

BK $*
