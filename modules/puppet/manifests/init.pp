class puppet {
  file { '/usr/local/bin/papply':
    source => 'puppet:///modules/puppet/papply.sh',
    mode   => '0755',
    owner  => root,
    group  => root,
  }
  cron { 'puppet.cron - papply':
    command => '/usr/local/bin/papply',
    user    => 'root',
    hour    => '*',
    minute  => '0',
  }
  exec { 'hiera.yaml':
    creates => '/etc/puppet/hiera.yaml',
    command => '/usr/bin/facter --yaml > /etc/puppet/hiera.yaml',
  }
}
