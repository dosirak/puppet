node default {
  include puppet
  include base
  include route53
  include '::ntp' 
}

node '_p1demap1' {
  group { 'git':
    ensure => 'present',
    gid    => '500',
  }
  user { 'git':
    ensure           => 'present',
    gid              => '500',
    groups           => ['git'],
    home             => '/home/git',
    password_max_age => '99999',
    password_min_age => '0',
    shell            => '/bin/false',
    uid              => '500',
  }
  file { '/home/git/':
    ensure => 'directory',
    owner  => 'git',
    group  => 'git',
  }

  group { 'wasadm':
    ensure => 'present',
    gid    => '510',
  }
  user { 'wasadm':
    ensure           => 'present',
    gid              => '510',
    groups           => ['wasadm'],
    home             => '/home/wasadm',
    password_max_age => '99999',
    password_min_age => '0',
    shell            => '/bin/bash',
    uid              => '510',
  }
  file { '/home/wasadm/':
    ensure => 'directory',
    owner  => 'wasadm',
    group  => 'wasadm',
  }

  group { 'tomcat8':
    ensure => 'present',
    gid    => '530',
  }
  user { 'tomcat8':
    ensure           => 'present',
    gid              => '530',
    groups           => ['tomcat8'],
    home             => '/home/tomcat8',
    password_max_age => '99999',
    password_min_age => '0',
    shell            => '/bin/bash',
    uid              => '530',
  }
  file { '/home/tomcat8/':
    ensure => 'directory',
    owner  => 'tomcat8',
    group  => 'tomcat8',
  }
}

node '_p2demap2' {
  # $ puppet module install puppetlabs-tomcat --version 1.6.1
  # Tomcat 7
  tomcat::install { '/opt/tomcat7':
    source_url => 'https://www-us.apache.org/dist/tomcat/tomcat-7/v7.0.77/bin/apache-tomcat-7.0.77.tar.gz',
  }
  tomcat::instance { 'default':
    catalina_home => '/opt/tomcat7',
  }

  # Tomcat 6
  tomcat::install { '/opt/tomcat6':
    source_url => 'http://www.apache.org/dist/tomcat/tomcat-6/v6.0.53/bin/apache-tomcat-6.0.53.tar.gz',
  }
  tomcat::instance { 'tomcat6':
    catalina_home => '/opt/tomcat6',
  }
  tomcat::config::server { 'tomcat6':
    catalina_base => '/opt/tomcat6',
    port          => '8105',
  }
  tomcat::config::server::connector { 'tomcat6-http':
    catalina_base         => '/opt/tomcat6',
    port                  => '8180',
    protocol              => 'HTTP/1.1',
    additional_attributes => {
      'redirectPort' => '8543'
    },
  }
  tomcat::config::server::connector { 'tomcat6-ajp':
    catalina_base         => '/opt/tomcat6',
    port                  => '8109',
    protocol              => 'AJP/1.3',
    additional_attributes => {
      'redirectPort' => '8543'
    },
  }
}
